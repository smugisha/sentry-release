# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.5.0

- minor: Upgrade sentry and dependencies

## 1.4.0

- minor: Use local script to create container with sentry
- patch: Fix sentry docker installtion

## 1.3.1

- patch: Fix errorsZ
- patch: Fix matching dashes

## 1.3.0

- minor: Add support for multiple projects

## 1.2.1

- patch: Fix environment variables for deployment

## 1.2.0

- minor: Add deployment tracking

## 1.1.2

- patch: Add sentry commit association

## 1.1.1

- patch: Fix docker image

## 1.1.0

- minor: Improve environment variable interface

## 1.0.2

- patch: Update readme with relevant instructions

## 1.0.1

- patch: Update readme with relevant instructions

## 1.0.0

- major: Add sentry release management

## 0.2.5

- patch: Internal maintenance: Add gitignore secrets.

## 0.2.4

- patch: Update the Readme with examples passing local environment variables to the remote host.

## 0.2.3

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.2.2

- patch: Updated readme about alternative base64 encoded SSH_KEY

## 0.2.1

- patch: Refactor pipe code to use pipes bash toolkit.

## 0.2.0

- minor: Update pipes toolkit version to avoid capturing the output into a variable when using run command. This prevents issues with large /dev/stdout output.

## 0.1.4

- patch: Documentation updates

## 0.1.3

- patch: Fixed the default value for MODE parameter

## 0.1.2

- patch: Update the yaml definition

## 0.1.1

- patch: Updated contributing guidelines

## 0.1.0

- minor: Initial release


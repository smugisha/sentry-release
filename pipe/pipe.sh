#!/usr/bin/env bash
#
# Run a command or script on your server
#
# Required globals:
#   SSH_USER
#   SERVER
#   COMMAND
#
# Optional globals:
#   DEBUG (default: "false")
#   MODE (default: "command")
#   SSH_KEY (default: null)
#   PORT (default: 22)
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

validate() {
    # required parameters
    : SSH_USER=${SSH_USER:?'SSH_USER variable missing.'}
    : SERVER=${SERVER:?'SERVER variable missing.'}
    : MODE=${MODE:="command"}
    : COMMAND=${COMMAND:?'COMMAND variable missing.'}
    : SENTRY_AUTH_TOKEN=${SENTRY_AUTH_TOKEN:?'SENTRY_AUTH_TOKEN variable missing.'}
    : SENTRY_ORG=${SENTRY_ORG:?'SENTRY_ORG variable missing.'}
    : DEBUG=${DEBUG:="false"}
    : SENTRY_PROJECTS=${SENTRY_PROJECTS:?'SENTRY_PROJECTS variable missing. List of projects split by comma(,).'}
    
    regex='^(([A-Za-z0-9-])+,?)+$'
    if [[ ! $SENTRY_PROJECTS =~ $regex ]]; then
        fail "Invalid $SENTRY_PROJECTS does not match format project1,project2,project3,..."
    fi
    
    if [ -z "$VERSION" ]; then
        VERSION="${BITBUCKET_COMMIT}"
    fi
}

setup_ssh_dir() {
    INJECTED_SSH_CONFIG_DIR="/opt/atlassian/pipelines/agent/ssh"
    # The default ssh key with open perms readable by alt uids
    IDENTITY_FILE="${INJECTED_SSH_CONFIG_DIR}/id_rsa_tmp"
    # The default known_hosts file
    KNOWN_SERVERS_FILE="${INJECTED_SSH_CONFIG_DIR}/known_hosts"
    
    mkdir -p ~/.ssh || debug "adding ssh keys to existing ~/.ssh"
    touch ~/.ssh/authorized_keys
    
    # If given, use SSH_KEY, otherwise check if the default is configured and use it
    if [ -n "${SSH_KEY}" ]; then
        info "Using passed SSH_KEY"
        (umask  077 ; echo ${SSH_KEY} | base64 -d > ~/.ssh/pipelines_id)
        elif [ ! -f ${IDENTITY_FILE} ]; then
        fail "No default SSH key configured in Pipelines."
    else
        info "Using default ssh key"
        cp ${IDENTITY_FILE} ~/.ssh/pipelines_id
    fi
    
    if [ ! -f ${KNOWN_SERVERS_FILE} ]; then
        fail "No SSH known_hosts configured in Pipelines."
    fi
    
    cat ${KNOWN_SERVERS_FILE} >> ~/.ssh/known_hosts
    
    if [ -f ~/.ssh/config ]; then
        debug "Appending to existing ~/.ssh/config file"
    fi
    echo "IdentityFile ~/.ssh/pipelines_id" >> ~/.ssh/config
    chmod -R go-rwx ~/.ssh/
}

prepare_release() {
    if [[ "${SENTRY_MODE}" = "off" ]]; then
        echo "sentry-cli releases new -p $(echo $SENTRY_PROJECTS | sed 's/,/ -p /g') $VERSION"
    else
        eval "sentry-cli releases new -p $(echo $SENTRY_PROJECTS | sed 's/,/ -p /g') $VERSION"
    fi
}

mark_released() {
    if [[ "${SENTRY_MODE}" = "off" ]]; then
        echo sentry-cli releases set-commits --auto $VERSION
        echo sentry-cli releases finalize "$VERSION"
    else
        sentry-cli releases set-commits --auto $VERSION
        sentry-cli releases finalize "$VERSION"
    fi
}

track_deployment() {
    start=$1
    now=$(date +%s)
    
    if [[ "${SENTRY_MODE}" = "off" ]]; then
        echo sentry-cli releases deploys $VERSION new -e $DEPLOYMENT_ENV -t $((now-start))
    else
        sentry-cli releases deploys $VERSION new -e $DEPLOYMENT_ENV -t $((now-start))
    fi
}

run_pipe() {
    start=$(date +%s)
    
    if [[ "${MODE}" = "command" ]]; then
        prepare_release
        info "Executing ${MODE} on ${SERVER}"
        run ssh -A -tt -i ~/.ssh/pipelines_id -o 'StrictHostKeyChecking=no' -p ${PORT:-22} $SSH_USER@$SERVER "$COMMAND"
        mark_released
        track_deployment $start
        elif [[ ${MODE} = "script" ]]; then
        prepare_release
        info "Executing script ${COMMAND} on ${SERVER}"
        run ssh -i ~/.ssh/pipelines_id -o 'StrictHostKeyChecking=no' -p ${PORT:-22} ${EXTRA_ARGS} $SSH_USER@$SERVER 'bash -s' < "$COMMAND"
        mark_released
        track_deployment $start
    else
        fail "Invalid MODE ${MODE}, valid values are: command, script."
    fi
    
    if [[ "${status}" == "0" ]]; then
        success "Execution finished."
    else
        fail "Execution failed."
    fi
}

validate
setup_ssh_dir
run_pipe
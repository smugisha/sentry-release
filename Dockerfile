FROM alpine:3.12

RUN apk --no-cache add \
    bash=5.0.17-r0 \
    openssh=8.3_p1-r0 \
    curl=7.69.1-r0

RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh

COPY setup .
RUN /bin/bash sentry 1.55.1
RUN rm -rf sentry

RUN sentry-cli --version

COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

ENTRYPOINT ["/pipe.sh"]
